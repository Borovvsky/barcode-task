<?php

namespace App\Enums;

enum BarcodeFormat: string {
    case JPG = 'jpg';
    case WEBP = 'webp';
    case PNG = 'png';
}

