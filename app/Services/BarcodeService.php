<?php

namespace App\Services;

use App\Enums\BarcodeFormat;
use App\Models\Barcode;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Picqer\Barcode\BarcodeGeneratorPNG;
use Intervention\Image\ImageManagerStatic as Image;

class BarcodeService
{
    final public const DEFAULT_QUALITY = 100;
    final public const DEFAULT_FORMAT = BarcodeFormat::WEBP;


    /**
     * @param string $barcodeTitle
     * @param string $barcodeString
     * @param int $quality
     * @param BarcodeFormat $format
     * @return void
     */
    public function saveBarcode(string $barcodeTitle, string $barcodeString, int $quality, BarcodeFormat $format): void
    {
        $filename = $this->generateUniqueFilename($format);
        $this->generateAndSaveBarcodeFile($barcodeString, $filename, $quality, $format->value);

        $barcode = new Barcode();
        $barcode->title = $barcodeTitle;
        $barcode->barcodeString = $barcodeString;
        $barcode->barcodePath = $filename;
        $barcode->save();
    }

    /**
     * @param Barcode $barcode
     * @param string $barcodeTitle
     * @param string|null $barcodeString
     * @param int $quality
     * @param BarcodeFormat $format
     * @return void
     */
    public function updateBarcode(Barcode $barcode, string $barcodeTitle, ?string $barcodeString, int $quality, BarcodeFormat $format): void
    {
        if ($barcodeString && $barcode->barcodeString !== $barcodeString) {
            Storage::disk('barcodes')->delete($barcode->barcodePath);
            $filename = $this->generateUniqueFilename($format);
            $this->generateAndSaveBarcodeFile($barcodeString, $filename, $quality, $format->value);
            $barcode->barcodeString = $barcodeString;
            $barcode->barcodePath = $filename;
        }
        $barcode->title = $barcodeTitle;
        $barcode->save();
    }

    /**
     * @param Barcode $barcode
     * @return void
     */
    public function deleteBarcode(Barcode $barcode): void
    {
        Storage::disk('barcodes')->delete($barcode->barcodePath);
        $barcode->delete();
    }

    /**
     * @param string $text
     * @param string $filename
     * @param int $quality
     * @param string $format
     * @return void
     */
    private function generateAndSaveBarcodeFile(string $text, string $filename, int $quality, string $format): void
    {
        try {
            $generator = new BarcodeGeneratorPNG();
            $barcodeData = $generator->getBarcode($text, $generator::TYPE_CODE_128);

            $image = Image::make($barcodeData)->encode($format, $quality);
            Storage::disk('barcodes')->put($filename, $image);
        } catch (\Exception $e) {
            Log::error("Error generating and saving barcode: {$e->getMessage()}");
        }
    }

    /**
     * @param BarcodeFormat $format
     * @return string
     */
    private function generateUniqueFilename(BarcodeFormat $format): string
    {
        return Str::uuid() . '.' . $format->value;
    }
}
