<?php

namespace App\Http\Controllers;

use App\Enums\BarcodeFormat;
use App\Http\Requests\BarcodeStoreRequest;
use App\Http\Requests\BarcodeUpdateRequest;
use App\Http\Resources\BarcodeCollection;
use App\Http\Resources\BarcodeResource;
use App\Models\Barcode;
use App\Services\BarcodeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class BarcodeController extends Controller
{
    /**
     * @param BarcodeService $barcodeService
     */
    public function __construct(
        private readonly BarcodeService $barcodeService
    ) {
    }

    /**
     * @return BarcodeCollection
     */
    public function index(): BarcodeCollection
    {
        return new BarcodeCollection(Barcode::all());
    }

    /**
     * @param BarcodeStoreRequest $barcodeRequest
     * @return JsonResponse
     */
    public function store(BarcodeStoreRequest $barcodeRequest): JsonResponse
    {
        try{
            $this->barcodeService->saveBarcode(
                $barcodeRequest->get('title'),
                $barcodeRequest->get('barcodeString'),
                $barcodeRequest->get('quality') ?? BarcodeService::DEFAULT_QUALITY,
                BarcodeFormat::tryFrom($barcodeRequest->get('format')) ?? BarcodeService::DEFAULT_FORMAT
            );
        } catch (\Throwable $e) {
            Log::error($e);

            return response()->json(['error' => 'An error occurred while creating the barcode.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([], Response::HTTP_CREATED);
    }

    /**
     * @param Barcode $barcode
     * @return BarcodeResource
     */
    public function show(Barcode $barcode): BarcodeResource
    {
        return new BarcodeResource($barcode);
    }

    /**
     * @param BarcodeUpdateRequest $barcodeUpdateRequest
     * @param Barcode $barcode
     * @return JsonResponse
     */
    public function update(BarcodeUpdateRequest $barcodeUpdateRequest, Barcode $barcode): JsonResponse
    {
        try {
            $this->barcodeService->updateBarcode(
                $barcode,
                $barcodeUpdateRequest->get('title'),
                $barcodeUpdateRequest->get('barcodeString'),
                $barcodeUpdateRequest->get('quality') ?? BarcodeService::DEFAULT_QUALITY,
                BarcodeFormat::tryFrom($barcodeUpdateRequest->get('format')) ?? BarcodeService::DEFAULT_FORMAT
            );
        } catch (\Throwable $e) {
            Log::error($e);

            return response()->json(['error' => 'An error occurred while updating the barcode.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([], Response::HTTP_OK);
    }

    /**
     * @param Barcode $barcode
     * @return JsonResponse
     */
    public function destroy(Barcode $barcode): JsonResponse
    {
        try {
            $this->barcodeService->deleteBarcode($barcode);
        } catch (\Throwable $e) {
            Log::error($e);

            return response()->json(['error' => 'An error occurred while deleting the barcode.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([], Response::HTTP_OK);
    }
}
