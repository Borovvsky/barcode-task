<?php

namespace App\Http\Requests;

use App\Enums\BarcodeFormat;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BarcodeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => [
                'required',
                'string',
                'max:255',
                Rule::unique('barcodes', 'title')->ignore($this->barcode)
            ],
            'barcodeString' => [
                'string',
                'max:255',
                Rule::unique('barcodes', 'barcodeString')->ignore($this->barcode)
            ],
            'quality' => [
                'nullable',
                'integer',
                'min:10',
                'max:100'
            ],
            'format' => [
                Rule::enum(BarcodeFormat::class),
                'nullable',
                'string',
                'max:6'
            ]
        ];
    }
}
