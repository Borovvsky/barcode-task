# Barcode application

- Postman collection in "postman" folder

## Step-by-step installation guide
- Copy `.env.example` to `.env`
- Start docker containers with `docker-compose up -d --build`
- Run `docker-compose exec php-fpm composer install`
- Run `docker-compose exec php-fpm php artisan key:generate`
- Run `docker-compose exec php-fpm php artisan migrate`
- Run `docker-compose exec php-fpm php artisan storage:link`
